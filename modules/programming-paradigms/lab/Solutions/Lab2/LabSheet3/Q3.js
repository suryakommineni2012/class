function toTitleCase(string) {
	//Split sentence into words
	var words = string.split(" ");

	for (var i = 0; i < words.length; i++) {
		//Capitalise the first letter
		words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1);
	}

	// Return the words array joined by spaces
	return words.join(" ");
}

console.log(toTitleCase("hello There, How Are you??"));