public class Context {

    RentingStrategy rentingStrategy;

    public RentingStrategy getTravelStrategy(Integer choice) {
        if (choice == 1){
            rentingStrategy = new Chauffeur();
        } else {
            rentingStrategy = new CarStrategy();
        }
        return rentingStrategy;
    }
}