# Alloy Analyzer

## Tools: 
- Alloy Analyzer: [http://alloytools.org/download.html](http://alloytools.org/download.html)  
- Java JDK: [https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html](https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html)
- Java JRE: [https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)

## Installation Instructions

1. For Alloy Analyzer, access the Download Link and choose alloy4.2.jar. Right click on the downloaded JAR file and click Open to open the Alloy Analyzer

2. For Java, use the download links for Windows and install them on to your machine. Follow these Instructions for Windows: [https://docs.oracle.com/en/java/javase/12/install/installation-jdk-microsoft-windows-platforms.html#GUID-DAF345BA-B3E7-4CF2-B87A-B6662D691840](https://docs.oracle.com/en/java/javase/12/install/installation-jdk-microsoft-windows-platforms.html#GUID-DAF345BA-B3E7-4CF2-B87A-B6662D691840)

## About this activity
Given a set of requirements in English, it is often necessary to formulate them in an declarative language so as to check for inconsistencies (if any), counterexamples and formulate stricter assertions so as to develop a model that is neither underconstrained nor overconstrained. As part of this activity, we will be using the declarative language Alloy to formally build a model and automatically check for counterexamples that violate the constraints of the system.

  
## Requirements

The requirements for our model are:

1.  Build a parent-child model representing humanity
    
2.  Every Human is either a Parent or a Child (or both, of course)
    
3.  There is exactly one Human named “Ancestor” who we assume to have no Parent, similar to a “root” node.
    
4.  Children of this ancestor are Humans
    
5.  Every Child has only one Parent
    
6.  The model is acylic, that is a parent cannot belong to the set of their children
    
7.  A mother and father to comprise a single parent instance so as to not overcomplicate the model
    

## Task

These requirements need to be formulated into assertions and facts as necessary, and then you need to formulate an assertion that is invalid and identify counterexamples. The invalid assertions needs to be fixed.
