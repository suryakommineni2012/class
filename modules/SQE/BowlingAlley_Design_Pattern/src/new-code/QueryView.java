import java.awt.*;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class QueryView implements ActionListener {
	private JFrame win;
	private int reverse = 0;
	private JButton addPatron, remPatron, finished;
	private JList partyList;
	private JLabel maxLabel, minLabel, playerLabel1, playerLabel2;
	private JTextField maxField, minField, playerField1, playerField2;
	private JComboBox jcb, newPatron;
	private QueryScore qs = new QueryScore();
	private int limit = 10;
	
	
	public QueryView() throws FileNotFoundException, IOException{
		
		
		int numberOfBowlers = qs.loadData().size();
		Vector mx = qs.getMax();
		Vector mi = qs.getMin();
		
		win = new JFrame("Query");
		win.getContentPane().setLayout(new BorderLayout());
		((JPanel) win.getContentPane()).setOpaque(false);

		JPanel newPatronPanel = new JPanel();
		newPatronPanel.setLayout(new GridBagLayout());

		Vector<String> choice = new Vector<>();
		choice.add("Query");
		choice.add("Maximum Scores");
		choice.add("Minimum Scores");
		choice.add("Top players");
		newPatron = new JComboBox(choice);
		newPatron.addActionListener(this);

		Vector<String> tmp = new Vector<>();
		int from = 0, x = 1;
		tmp.add("Select");
		while(from < numberOfBowlers){
			int d = limit*(x-1) + 1;
			tmp.add(d + "-" + limit*x);
			from += limit;x++;
		}
		jcb = new JComboBox(tmp);
		jcb.addActionListener(this);
		
		Vector empty = new Vector();
		empty.add("-");
		partyList = new JList(empty);
		partyList.setVisibleRowCount(15);
//		partyList.addListSelectionListener(this);
		JScrollPane partyPane = new JScrollPane(partyList);
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;	
		gbc.gridx = 0;
		gbc.gridy = 0;
		newPatronPanel.add(newPatron, gbc);
		gbc.fill = GridBagConstraints.HORIZONTAL;	
		gbc.gridx = 0;
		gbc.gridy = 1;
		newPatronPanel.add(new JPanel(), gbc);
		gbc.fill = GridBagConstraints.HORIZONTAL;	
		gbc.gridx = 0;
		gbc.gridy = 2;
		newPatronPanel.add(jcb, gbc);
		gbc.fill = GridBagConstraints.HORIZONTAL;	
		gbc.gridx = 0;
		gbc.gridy = 3;
		newPatronPanel.add(new JPanel(), gbc);
		gbc.fill = GridBagConstraints.HORIZONTAL;	
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridheight = 4;
		newPatronPanel.add(partyList, gbc);

		win.getContentPane().add("Center", newPatronPanel);
		win.pack();

		// Center Window on Screen
		win.setSize(300, 300);
		Dimension screenSize = (Toolkit.getDefaultToolkit()).getScreenSize();
		win.setLocation(
			((screenSize.width) / 2) - ((win.getSize().width) / 2),
			((screenSize.height) / 2) - ((win.getSize().height) / 2));
		win.show();
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(newPatron)){
			String selected = newPatron.getSelectedItem().toString();
			if("Query".equals(selected)){
				// do nothing
			}else {
				if("Minimum Scores".equals(selected)) reverse = 0;
				else if("Maximum Scores".equals(selected)) reverse = 1;
				else {
					reverse = 2;
					Vector<Pair> mx;
					try {
						mx = qs.getTopPlayer(0, 0, reverse);
						Vector<String> temp = new Vector<String>();
						for(int i=0;i<mx.size();i++) temp.add(mx.get(i).x + " " + mx.get(i).y);
						partyList.setListData(temp);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			}
		}
		if(e.getSource().equals(jcb)){
			String selected = jcb.getSelectedItem().toString();
			if("Select".equals(selected)){
				// do nothing.
			}else{
				Integer last = Integer.valueOf(selected.split("-")[1]);
				try {
					Vector<Pair> mx = qs.getTopPlayer(last-limit+1, last, reverse);
					Vector<String> temp = new Vector<String>();
					for(int i=0;i<mx.size();i++) temp.add(mx.get(i).x + " " + mx.get(i).y);
					partyList.setListData(temp);
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}

}
