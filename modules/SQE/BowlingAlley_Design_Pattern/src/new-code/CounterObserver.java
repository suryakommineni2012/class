public interface CounterObserver {

	public void receiveCounterEvent(int x);
};

